$('header').bind('inview', function (e, isInView) {
  if (isInView) {
    $('.h1').removeClass('hide');
    $('.h1').addClass('animated');

    $('.intro').removeClass('hide');
    $('.intro').addClass('animated');
  } else {
    $('.h1').removeClass('animated');
    $('.h1').addClass('hide');

    $('.intro').removeClass('animated');
    $('.intro').addClass('hide');
  }
});

$('.options').bind('inview', function (e, isInView) {
  if (isInView) {
    $('.options li').removeClass('hide');
    $('.options li').addClass('animated');
  } else {
    $('.options li').removeClass('animated');
    $('.options li').addClass('hide');
  }
});

$('.main').bind('inview', function (e, isInView) {
  if (isInView) {
    $('.main div').removeClass('hide');
    $('.main div').addClass('animated');
  } else {
    $('.main div').removeClass('animated');
    $('.main div').addClass('hide');
  }
});

$('.cuisines').bind('inview', function (e, isInView) {
  if (isInView) {
    $('.recipes1 li').removeClass('hide');
    $('.recipes1 li').addClass('animated');

    $('.recipes2 li').removeClass('hide');
    $('.recipes2 li').addClass('animated');
  } else {
    $('.recipes1 li').removeClass('animated');
    $('.recipes1 li').addClass('hide');

    $('.recipes2 li').removeClass('animated');
    $('.recipes2 li').addClass('hide');
  }
});

$('.counters').bind('inview', function (e, isInView) {
  $('.counters li').removeClass('hide');

  $('.counters li').addClass('animated');

  $('.count1').animateNumber(
    {
      number: 23567,

      numberStep: function (now, tween) {
        var floored_number = Math.floor(now), target = $(tween.elem);

        target.text(floored_number.toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }
    },
    1500
  );
  $('.count2').animateNumber(
    {
      number: 431279,

      numberStep: function (now, tween) {
        var floored_number = Math.floor(now), target = $(tween.elem);

        target.text(floored_number.toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }
    },
    1500
  );
  $('.count3').animateNumber(
    {
      number: 892173,

      numberStep: function (now, tween) {
        var floored_number = Math.floor(now), target = $(tween.elem);

        target.text(floored_number.toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }
    },
    1500
  );

  $('.count4').animateNumber(
    {
      number: 56581,

      numberStep: function (now, tween) {
        var floored_number = Math.floor(now), target = $(tween.elem);

        target.text(floored_number.toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }
    },
    1500
  );

  $('.count5').animateNumber(
    {
      number: 3183,

      numberStep: function (now, tween) {
        var floored_number = Math.floor(now), target = $(tween.elem);

        target.text(floored_number.toFixed().replace(/\B(?=(\d{3})+(?!\d))/g, ","));
      }
    },
    1500
  );
});
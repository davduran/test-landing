# PRUEBA FRONTEND PLASTIC

## Estructura Html.

Se ha creado una estructura HTML a la que aplicarle estilos y funcionalidades. Se puede modificar, añadir, mover o eliminar los elementos tanto como se necesite. 

También están cortadas las imágenes necesarias para montar la página. Se adjunta el PSD por si fuera necesario recortar algún elemento de otra forma.

## Estilos y scripts. 

No se ha añadido ningún estilo ni script. Se pueden utilizar las librerías que se necesiten (jQuery, plugins, etc) así como utilizar cualquier pre compilador de estilos (sass, less, etc) o cualquier frameworks (bootstrap, grids, etc).

## Responsive

La página ha ser repsonsive:
- Desktop: Siguiendo el diseño del PSD
- Movil: Adaptando los estilos para verlo correctamente en movil. 